# Generate raster tiles from GeoTIFF

This is an example how to generate raster tiles with Mapserver + Mapproxy with the following properties:

- EPSG:28992 projection
- Bounding box of [123000,498000,130500,505000] (Purmerend)
- Zoom levels 5 - 16

## Getting started

First copy the GeoTIFF to data/lufo.tif. Then generate an index with:

```bash
cd data/
gdaltindex imagery.shp lufo.tif
```

Then start up Mapserver + Mapproxy in the background with:

```bash
docker-compose up -d
```

Now start generate the tiles with:

```bash
docker-compose exec mapproxy mapproxy-seed -c 4 -s /srv/mapproxy/config/seed.yaml -f /srv/mapproxy/config/mapproxy.yaml --seed=lufo
```

The resulting tiles wll be saved in output-tiles/lufo_cache_EPSG28992. You can upload this to an object store with [Rclone](https://rclone.org/).
